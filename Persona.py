class Persona:
	def __init__ (self, nom, ape, nac, dni):
		self._nombre = nom
		self._apellidos = ape
		self._fecha_de_nacimiento = nac
		self._dni = dni
		
	def setNombre (self, a):
		self._nombre = a
		
	def getNombre (self):
		return self._nombre
		
	def setApellidos (self, b):
		self._apellidos = b
		
	def getApellidos (self):
		return self._apellidos
		
	def setFechaNac (self, c):
		self._fecha_de_nacimiento = c
		
	def getFechaNac (self):
		return self._fecha_de_nacimiento
		
	def setDNI (self, d):
		self._dni = d
		
	def getDNI (self):
		return self._dni
			
	def __str__ (self):
		return "El paciente {nombre} {apellidos}, con fecha de nacimiento {fecha} y DNI {DNI}".format(nombre = self._nombre, apellidos = self._apellidos, fecha = self._fecha_de_nacimiento, DNI = self._dni)
			

class Paciente (Persona):
	def __init__ (self, nom, ape, nac, dni, hist):
		super().__init__(nom, ape, nac, dni)
		self.historial_clinico = hist
		
	def ver_historial_clinico (self):
		return "Historial clinico del paciente: {}".format(self.historial_clinico)


class Medico (Persona):
	def __init__ (self, nom, ape, nac, dni, espec, citas):
		super().__init__(nom, ape, nac, dni)
		self.especialidad = espec
		self.citas = citas
		
	def consultar_agenda (self):
		if len(self.citas) != 0:
			agenda = "Las citas programadas son:\n"
			for cita in self.citas:
				agenda += cita + "\n"
			return agenda			
		else:
			return "No hay citas programadas"

		
'''persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
print(persona1)'''

'''paciente1 = Paciente("Juan", "Perez Florez", "13/08/2004", "12345678R", "Alergia a carne de pescado")
print(paciente1.ver_historial_clinico())'''

'''citas = ["19/12/2024 a las 14:00", "20/12/2024 a las 15:00"]
medico1 = Medico("Juan", "Perez Florez", "13/08/2004", "12345678R", "cardiólogo", citas)'''