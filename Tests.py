import unittest
from Persona import Persona
from Persona import Paciente
from Persona import Medico

class TestPersona (unittest.TestCase):
	def test_set_nombre (self):
		persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
		persona1.setNombre("Julio")
		self.assertEqual(persona1._nombre, "Julio")
		
	def test_get_nombre (self):
		persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
		self.assertEqual(persona1.getNombre(), "Juan")
		
	def test_set_apellidos (self):
		persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
		persona1.setApellidos("Ramirez Delgado")
		self.assertEqual(persona1._apellidos, "Ramirez Delgado")
		
	def test_get_apellidos (self):
		persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
		self.assertEqual(persona1.getApellidos(), "Perez Florez")
		
	def test_set_fechaNac (self):
		persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
		persona1.setFechaNac("12/07/2005")
		self.assertEqual(persona1._fecha_de_nacimiento, "12/07/2005")
		
	def test_get_fechaNac (self):
		persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
		self.assertEqual(persona1.getFechaNac(), "13/08/2004")
		
	def test_set_dni (self):
		persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
		persona1.setDNI("99999999X")
		self.assertEqual(persona1._dni, "99999999X")
		
	def test_get_dni (self):
		persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
		self.assertEqual(persona1.getDNI(), "12345678R")

	def test_str (self):
		persona1 = Persona("Juan", "Perez Florez", "13/08/2004", "12345678R")
		self.assertEqual(persona1.__str__(), "El paciente Juan Perez Florez, con fecha de nacimiento 13/08/2004 y DNI 12345678R")		
		
		
class TestPaciente (unittest.TestCase):
	def test_ver_historial_clinico (self):
		paciente1 = Paciente("Juan", "Perez Florez", "13/08/2004", "12345678R", "Alergia a carne de pescado")
		self.assertEqual(paciente1.ver_historial_clinico(), "Historial clinico del paciente: Alergia a carne de pescado")


class TestMedico (unittest.TestCase):
	def test_consultar_agenda_con_citas (self):
		citas = ["19/12/2024 a las 14:00", "20/12/2024 a las 15:00"]
		medico1 = Medico("Juan", "Perez Florez", "13/08/2004", "12345678R", "cardiólogo", citas)
		self.assertEqual(medico1.consultar_agenda(), "Las citas programadas son:\n19/12/2024 a las 14:00\n20/12/2024 a las 15:00\n")
	
	def test_consultar_agenda_sin_citas (self):
		citas = []
		medico1 = Medico("Juan", "Perez Florez", "13/08/2004", "12345678R", "cardiólogo", citas)
		self.assertEqual(medico1.consultar_agenda(), "No hay citas programadas")

if __name__ == '__main__':
	unittest.main()